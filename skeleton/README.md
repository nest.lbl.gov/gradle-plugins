# Gradle Skeleton Plugin #

# Overview #

This plugin is designed to be cloned whenever a new plugin it needed. It already has all of the peices needed to build and deploy a plugin. In general all that is needed after this sub-project has been copied it to change every occurance of '[Ss]keleton' with the name of the new plugin.

