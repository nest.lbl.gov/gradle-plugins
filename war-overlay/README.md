# Gradle WAR overlay plugin #

---
This plugin replicates some of the behavior of the [Maven WAR Plugin's Overlay facility](https://maven.apache.org/plugins/maven-war-plugin/overlays.html).
---

## Overview ##

This plugin extends the standard gradle [WAR plugin](https://docs.gradle.org/current/userguide/war_plugin.html) by 