package gov.lbl.nest.plugin.overlay;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.gradle.api.Action;
import org.gradle.api.NamedDomainObjectContainer;
import org.gradle.api.Plugin;
import org.gradle.api.Project;
import org.gradle.api.ProjectConfigurationException;
import org.gradle.api.Task;
import org.gradle.api.file.CopySpec;
import org.gradle.api.file.DuplicatesStrategy;
import org.gradle.api.internal.file.copy.CopySpecInternal;
import org.gradle.api.plugins.WarPlugin;
import org.gradle.api.tasks.TaskCollection;
import org.gradle.api.tasks.bundling.War;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A simple 'hello world' plugin.
 */
public class WarOverlayPlugin implements
                              Plugin<Project> {

    /**
     * The {@link Logger} used by this class.
     */
    private static final Logger LOG = LoggerFactory.getLogger(WarOverlayPlugin.class);

    private static final int PARTS_GROUP_ID = 0;

    private static final int PARTS_ARTIFACT_ID = PARTS_GROUP_ID + 1;

    private static final int PARTS_VERSION = PARTS_ARTIFACT_ID + 1;

    private static final int PARTS_LENGTH = PARTS_VERSION + 1;

    private static final String WAR_ARTIFACT_STRING = "@war";

    public void apply(Project project) {
        // Apply the WAR plugin that this class is extending.
        (project.getPluginManager()).apply(WarPlugin.class);
        NamedDomainObjectContainer<Overlay> overlays = project.container(Overlay.class);
        (project.getExtensions()).add("overlays",
                                      overlays);

        // Add an action to the end of the war creation.
        final TaskCollection<War> tasks = ((project.getTasks()).withType(War.class));
        for (War task : tasks) {
            final CopySpecInternal rootSpec = task.getRootSpec();
            final CopySpec spec = rootSpec.addChild();

            task.doFirst(new Action<Task>() {

                @Override
                public void execute(Task task) {

                    final Set<String> names = overlays.getNames();
                    for (String name : names) {
                        final Overlay overlay = overlays.getByName(name);
                        final String source = overlay.getSource();
                        final String[] parts = source.split(":");
                        if (PARTS_LENGTH != parts.length) {
                            throw new ProjectConfigurationException("Source must be specified as \"groupId:artifactId:version\"",
                                                                    (Throwable) null);
                        }
                        final String versionToUse;
                        if (parts[PARTS_VERSION].endsWith(WAR_ARTIFACT_STRING)) {
                            versionToUse = parts[PARTS_VERSION].substring(0,
                                                                          (parts[PARTS_VERSION].length() - WAR_ARTIFACT_STRING.length()));
                        } else {
                            versionToUse = parts[PARTS_VERSION];
                        }
                        final String warName = parts[PARTS_ARTIFACT_ID] + "-"
                                               + versionToUse
                                               + "."
                                               + War.WAR_EXTENSION;
                        final File warFile = WarOverlayPlugin.findWarFile((project.getConfigurations()).getByName("compileClasspath"),
                                                                          warName);
                        if (null == warFile) {
                            throw new ProjectConfigurationException("Source WAR not provided in classpath",
                                                                    (Throwable) null);
                        }
                        rootSpec.exclude(warFile.getName());
                        final CopySpec overlaySpec = spec.from(project.zipTree(warFile));

                        // Process exclusions
                        final List<String> exclusions = new ArrayList<String>();
                        final List<String> requestedExcludes = overlay.getExcludes();
                        if (null != requestedExcludes && !requestedExcludes.isEmpty()) {
                            LOG.debug("Exclude elements for Overlay " + name);
                            exclusions.addAll(requestedExcludes);
                            for (String exclude : exclusions) {
                                LOG.debug(" - " + exclude);
                            }
                        }
                        exclusions.add("META-INF/MANIFEST.MF");
                        spec.exclude(exclusions);

                        // Process inclusions
                        final List<String> inclusions = new ArrayList<String>();
                        final List<String> requestedIncludes = overlay.getIncludes();
                        if (null != requestedIncludes && !requestedIncludes.isEmpty()) {
                            LOG.debug("Include elements for Overlay " + name);
                            inclusions.addAll(requestedIncludes);
                            for (String include : requestedIncludes) {
                                LOG.debug(" - " + include);
                            }
                            overlaySpec.include(inclusions);
                        }

                        final DuplicatesStrategy strategy = overlay.getStrategy();
                        if (null != strategy) {
                            LOG.debug("DuplicatesStrategy for Overlay " + name
                                      + " declared as "
                                      + strategy.toString());
                        }
                    }
                }
            });
        }

        /*
         * This has been left here until the functional test can be modified to confirm
         * the "real" behavior of this plugin.
         */
        (project.getTasks()).register("overlay",
                                      task -> {
                                          task.doLast(s -> System.out.println("Hello from plugin 'gov.lbl.nest.skeleton'"));
                                      });
    }

    private static File findWarFile(Iterable<File> files,
                                    String warName) {
        for (File file : files) {
            if (warName.equals(file.getName())) {
                return file;
            }
        }
        return null;
    }

}
