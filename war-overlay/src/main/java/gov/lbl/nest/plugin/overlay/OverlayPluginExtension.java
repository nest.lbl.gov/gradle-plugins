package gov.lbl.nest.plugin.overlay;

import org.gradle.api.provider.Property;

abstract public class OverlayPluginExtension {

    abstract public Property<String> getOverlay();
}