package gov.lbl.nest.plugin.overlay;

import java.util.List;

import javax.inject.Inject;

import org.gradle.api.file.DuplicatesStrategy;

abstract public class Overlay {

    private List<String> excludes;

    private List<String> includes;

    private String name;

    private DuplicatesStrategy strategy;

    private String source;

    private String targetPath;

    @Inject
    public Overlay(String name) {
        this.name = name;
    }

    public List<String> getExcludes() {
        return excludes;
    }

    public List<String> getIncludes() {
        return includes;
    }

    public String getName() {
        return name;
    }

    public DuplicatesStrategy getStrategy() {
        return strategy;
    }

    public String getSource() {
        return source;
    }

    public String getTargetPath() {
        return targetPath;
    }

    public void setExcludes(List<String> excludes) {
        this.excludes = excludes;
    }

    public void setIncludes(List<String> includes) {
        this.includes = includes;
    }

    public void setStrategy(DuplicatesStrategy strategy) {
        this.strategy = strategy;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public void setTargetPath(String targetPath) {
        this.targetPath = targetPath;
    }
}
