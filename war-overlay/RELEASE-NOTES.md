# Release notes #


## Version 0.0.5 ##

- Find WAR file from `classpath`, given "group:artifact:version" as source.


## Version 0.0.4 ##

- Add in  configuration equivalents to Maven elements:
  - excludes - the files to exclude. By default, the META-INF/MANIFEST.MF file is excluded.
  - includes - the files to include. By default, all files are included.
  - targetPath - the target relative path in the webapp structure. By default, the content of the overlay is added in the root structure of the webapp.

- Add `gradle` specific option:
  - strategy - the `org.gradle.api.file.DuplicatesStrategy` to be used during the overlay.


## Version 0.0.3 ##

- Specify overlay WARs as a DSL block in the `build.gradle` file.


## Version 0.0.2 ##

- Add single WAR to CopySpec for the WAR task.


 ## Version 0.0.1 ##

- Find the WAR artifact into which the overlay will be copied.
